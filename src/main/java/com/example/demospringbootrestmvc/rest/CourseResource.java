package com.example.demospringbootrestmvc.rest;

import com.example.demospringbootrestmvc.model.Course;
import com.example.demospringbootrestmvc.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8081"})
@RestController
public class CourseResource {

    private static final Logger log = LoggerFactory.getLogger(CourseResource.class);

    private final CourseService courseService;

    public CourseResource(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping(path = "/instructors/{username}/courses")
    public List<Course> getListCourse(@PathVariable String username) {
        log.info("Username = {}", username);
        return courseService.findAllCourses();
    }

}
