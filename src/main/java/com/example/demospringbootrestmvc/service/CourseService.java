package com.example.demospringbootrestmvc.service;

import com.example.demospringbootrestmvc.model.Course;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class CourseService {

    private static List<Course> courses;

    static {
        courses = Arrays.asList(
                new Course(1, "A"),
                new Course(2, "B"),
                new Course(3, "C"));
    }

    public List<Course> findAllCourses() {
        return courses;
    }

}
