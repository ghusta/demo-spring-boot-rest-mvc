package com.example.demospringbootrestmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringBootRestMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringBootRestMvcApplication.class, args);
    }

}
